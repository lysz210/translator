<?php

class UnknownResource implements INode {
    function read(){
        return null;
    }

    function rewind(){
        return;
    }

    function close(){
        return;
    }

    function isUnknown(){
        return true;
    }

    function size(){
        return 0;
    }

    function getPath(){
        return null;
    }
}
 ?>
