<?php
include_once(".__init__.php");

define('CMD_CREA_DIZIONARIO', 'CREA_DIZIONARIO');
define('CMD_LISTA_DIZIONARI', 'LISTA_DIZIONARY');
define('CMD_GET_DIZIONARIO', 'GET_DIZIONARIO');
define('CMD_INSERISCI', 'INSERISCI');
define('CMD_ELIMINA', 'ELIMINA');

define('NAME_LANGLIST', '.lista_lingue.json');
define('NAME_CONFIG', '.config.json');
define('NAME_KEYFILE', '.lista_chiavi.json');

$command = CMD_CREA_DIZIONARIO;
$command = CMD_LISTA_DIZIONARI;
$lingua = "en";
// echo "start app";
try{
    switch ($command) {
        case CMD_CREA_DIZIONARIO:
            createDictionary($lingua);
            $res->addMessage( 'Dizionario creato correttamente');
            break;
        case CMD_LISTA_DIZIONARI:
            $lista = getAvailableDictionary();
            $res->addData('dizionary', $lista);
            $res->addData('quantita', count($lista));
            break;
        case CMD_GET_DIZIONARIO:
            $d = getDictionary($lingua, $dizionario);
            $res->addData('lingua', $lingua);
            $res->addData('dizionario', $d);
            break;
        case CMD_INSERISCI:
            insert($lingua, $dizionario, $key, $cont);
            break;
        case CMD_ELIMINA:
            delete($lingua, $key);
            break;
    }
} catch( LangNameException $e ) {
    $msg = LangNameException::ERR_NAME . '::' . $e->getType() . ":" . $e->getMessage();
    $res->setError($msg);
} catch( Exception $e ) {
    $res->setError($e->getMessage());
}

$res->ends();

function createDictionary($lingua){
    if(!is_string($lingua)) {
        throw LangNameException::create("Nome inserito non valido, il nome deve essere una stringa lunga almeno 2 caratteri.", "Param type err");
    } if(strlen($lingua) < 2){
        throw LangNameException::create("Nome inserito non valido, il nome inserito e' troppo corto, la parola deve essere lunga almeno 2 caratteri.", "Param length err");
    }
    if(file_exists(LANG_DIR . $lingua)){
        return;
    }
    $baseDir = new MyDir(LANG_DIR);
    $langDir = $baseDir->mkdir($lingua);
    // $langDir = $baseDir->open($lingua);
    $flanglist = $baseDir->openfile(NAME_LANGLIST);
    $langlist = json_decode($flanglist->readAll());
    //var_dump($langlist);
    $cfg = array(
        'nome' => $lingua,
    );
    $cfgfilecont = json_encode($cfg, JSON_PRETTY_PRINT);
    if(!$cfgfilecont){
        throw new Exception("Errore creazione del file di configurazione");
    }
    $fconf = $langDir->touch(NAME_CONFIG);
    $fconf->write($cfgfilecont);
    $langlist[] = $lingua;
    $langlist = array_unique($langlist);
    $flanglist->write(json_encode($langlist, JSON_PRETTY_PRINT));
}

function getAvailableDictionary(){
    $baseDir = new MyDir(LANG_DIR);
    $langs = $baseDir->openfile(NAME_LANGLIST);
    return json_decode($langs->readAll(), true);
}

function getDictionary($lingua, $dizionario){
    $langDir = LANG_DIR . $lingua;
    if(!file_exists($langDir)){
        throw LangNotAvailableException::create("La lingua inserita non e' dispobile.");
    }
    $cfg = json_decode(file_get_contents($langDir . DIRECTORY_SEPARATOR . NAME_CONFIG), true);
    $dizionario = $langDir . DIRECTORY_SEPARATOR . $dizionario . "." . LANG_FILE_TYPE;
    $dcontent = file_get_contents($dizionario);
    if(!dcontent){
        throw DictNotAvailableException::create("Il dizionario scelto non e' disponibile.");
    }
    $d = json_decode($dcontent);
    if(!$d){
        throw new Exception("Errore nella decodifica del dizionario.");
    }
    return $d;
}

function insert($lingua, $dizionario, $key, $val){
    //TODO creare la funzione per inserire una nuova voce
    $langDir = LANG_DIR . $lingua;
    if(file_exists($langDir)){

    }
}

function delete($lingua, $dizionario, $key){
    //TODO creare la funzione per eliminare una voce
}
 ?>
