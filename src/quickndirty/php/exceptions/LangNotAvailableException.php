<?php

class LangNotAvailableException extends Exception {
    const ERR_NAME = "LangNotAvailableException";
    private $_type = "LangNotAvailableException";
    function setType($type){
        $this->_type = $type;
    }
    function getType(){ return $this->_type; }

    static function create($msg, $type){
        $e = new LangNameException($msg);
        if($type){
            $e->setType($type);
        }
        return $e;
    }
}
?>
