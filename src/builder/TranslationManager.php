<?php  namespace just1team\translator\builder;

/**
 * TranslationManager e' un classe
 * per la gestione della traduzione
 */
class TranslationManager {

    private $_availableDictrionaries = array();

    /**
     * il path in cui e' residente il dizionario
     * @var string
     */
    private $_dictionaryPath = null;

    /**
     * costruttore
     * richiede il path dove si trovera' il dizionario
     * @param string $path path della residenza del dizionario
     */
    function __construct(string $path) {
        $this->_dictionaryPath = $path;
        $this->_detectDictionaries();
    }

    /**
     * verifica quali dizionari sono presenti all'interno del traduttore
     */
    private function _detectDictionaries(){

    }

    /**
     * recupera il dizionario con la lingua e il territorio specificato
     * la lingua e' obbligatoria mentre il territorio e' opzionale,
     * nel caso non venga specificato allora verra restituito quello di default
     * @param  string $language  lingua di cui si vuole il dizionario per la traduzione
     * @param  string $territory territorio specifico per la lingua
     * @return Dictionary            dizionario richiesto
     */
    function getDictionary(string $language, string $territory="defaut"){
        $dictionary = null;
        return $dictionary;
    }

    function __toString(){

    }

}


 ?>
