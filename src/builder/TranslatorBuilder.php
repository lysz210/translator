<?php namespace just1team\translator\builder;

/**
 * TranslatorBuilder e' una semplice classe per la creazione di un dizionario
 */
class TranslatorBuilder {

    private $_path = null;

    /**
     * costruttore
     * setta il path dove si trova il dizionario
     * @param string $path path in cui si trovano o verrano inseriti i dati del dizionario
     */
    function __construct(string $path){
        $this->path = $path;
    }

    /**
     * crea un nuovo oggetto TranslationManager
     * @return TranslationManager gestore per i dizionari
     */
    function create(){
        $manager = new TranslationManager($this->_path);
        return $manager;
    }
}

?>
